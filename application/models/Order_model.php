<?php
class Order_model extends CI_Model{
  public $nama;
  public $alamat;
  public $mobile_number;
  public $kebutuhan;
  public $hari_tanggal;
  public $pukul;
  public $create_at;
  public $update_at;

  public function getDataOrder()
  {
    $this->load->database();
    $orders = $this->db->get("pemesanan");
    $result = $orders->result();
    return json_encode ($result);
  }
}

